/*
 * 系统名称: 
 * 模块名称: webpasser.core
 * 类 名 称: Rule.java
 *   
 */
package com.hxt.webpasser.transport.xml;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2015-9-9 <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
@XStreamAlias("rule")
public class Rule {

	@XStreamAsAttribute
	private String type;
	@XStreamAsAttribute
	private String value;
	@XStreamAsAttribute
	private String attr;
	
	@XStreamAsAttribute
	private String exp;
	
	@XStreamAsAttribute
	private String classPath;
	
	
	private String pre;
	
	@XStreamAlias("end")
	private String endParam;
	
	private String oldChars;
	private String newChars;
	
	@XStreamImplicit(itemFieldName="tag")
	private List<String> tags;
	 @XStreamImplicit(itemFieldName="param")
	private List<Param> params;
	 
	 /**
	     foreach 用
	                    */
	 @XStreamAsAttribute
	 private String items;
	 
	 @XStreamAsAttribute()
	 private String begin;
	 
	 @XStreamAsAttribute()
	 private String ends;
	 
	 @XStreamAsAttribute
	 private String step;
	 
	 /**
     compareCheck 用
                    */
	 
	/* @XStreamAsAttribute
	 private String leftValue;
	 
	 @XStreamAsAttribute
	 private String rightValue;
	 
	 @XStreamAsAttribute
	 private String operator;*/
	 
	 /**
	  * method用
	  */
	 @XStreamAsAttribute
	 private String method;
	 
	 
	 
	 @XStreamImplicit(itemFieldName="ifConditionList")
	private List<IfConditionList> ifConditionLists;
		
	@XStreamImplicit(itemFieldName="ifCondition")
	private List<IfCondition> ifConditions;
	 
	 
	 private List<Rule> rules;
	 
	 
	 
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public String getClassPath() {
		return classPath;
	}
	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}
	public String getPre() {
		return pre;
	}
	public void setPre(String pre) {
		this.pre = pre;
	}
	/*
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	*/
	public String getOldChars() {
		return oldChars;
	}
	public void setOldChars(String oldChars) {
		this.oldChars = oldChars;
	}
	public String getNewChars() {
		return newChars;
	}
	public void setNewChars(String newChars) {
		this.newChars = newChars;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public String getExp() {
		return exp;
	}
	public void setExp(String exp) {
		this.exp = exp;
	}
	public List<Param> getParams() {
		return params;
	}
	public void setParams(List<Param> params) {
		this.params = params;
	}
	public String getEndParam() {
		return endParam;
	}
	public void setEndParam(String endParam) {
		this.endParam = endParam;
	}
	public String getItems() {
		return items;
	}
	public void setItems(String items) {
		this.items = items;
	}
	public String getBegin() {
		return begin;
	}
	public void setBegin(String begin) {
		this.begin = begin;
	}
	
	public String getEnds() {
		return ends;
	}
	public void setEnds(String ends) {
		this.ends = ends;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	public List<Rule> getRules() {
		return rules;
	}
	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}
	public List<IfConditionList> getIfConditionLists() {
		return ifConditionLists;
	}
	public void setIfConditionLists(List<IfConditionList> ifConditionLists) {
		this.ifConditionLists = ifConditionLists;
	}
	public List<IfCondition> getIfConditions() {
		return ifConditions;
	}
	public void setIfConditions(List<IfCondition> ifConditions) {
		this.ifConditions = ifConditions;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
	
	
	
}
