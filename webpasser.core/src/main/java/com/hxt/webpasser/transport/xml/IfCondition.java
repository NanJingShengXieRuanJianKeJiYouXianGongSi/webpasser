package com.hxt.webpasser.transport.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
@XStreamAlias("ifCondition")
public class IfCondition {

	@XStreamAsAttribute
	private String logicOperator;
	 
	 @XStreamAsAttribute
	 private String leftValue;
	 
	 @XStreamAsAttribute
	 private String rightValue;
	 
	 @XStreamAsAttribute
	 private String compareOperator;

	public String getLogicOperator() {
		return logicOperator;
	}

	public void setLogicOperator(String logicOperator) {
		this.logicOperator = logicOperator;
	}

	public String getLeftValue() {
		return leftValue;
	}

	public void setLeftValue(String leftValue) {
		this.leftValue = leftValue;
	}

	public String getRightValue() {
		return rightValue;
	}

	public void setRightValue(String rightValue) {
		this.rightValue = rightValue;
	}

	public String getCompareOperator() {
		return compareOperator;
	}

	public void setCompareOperator(String compareOperator) {
		this.compareOperator = compareOperator;
	}
	 
	 
	
	
	
}
