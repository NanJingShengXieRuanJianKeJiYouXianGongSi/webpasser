/**
 */
package com.hxt.webpasser.regular;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hxt.webpasser.transport.xml.Rule;
import com.hxt.webpasser.utils.FillParamValueUtil;
import com.hxt.webpasser.utils.StringUtil;

/**   
 * @ClassName:  MethodRule  
 * @Description:  其他函数的规则
 * @author: hanxuetong
 * @date:   2018年7月12日
 * @version V1.0
 */
public class MethodRule implements DecideRule{
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	/* (non-Javadoc)
	 * @see com.hxt.webpasser.regular.DecideRule#handle(com.hxt.webpasser.transport.xml.Rule, java.util.List, java.util.Map)
	 */
	public List<Object> handle(Rule rule, List<Object> contentList, Map valueMap) {

		String method=rule.getMethod();
		if(contentList!=null&&method!=null)
		{
			for(int i=0;i<contentList.size();i++)
			{
				String con=StringUtil.toString(contentList.get(i));
				con=getMethodValue(con, method);
				contentList.set(i, con);
			}	
		}

		return contentList;
	}

	
	public String getMethodValue(String val,String method){
		if(val==null){
			return null;
		}
		if("urlencode".equals(method)){
			String result = URLEncoder.encode(val); 
			return result;
		}else if("urldecode".equals(method)){
			String result = URLDecoder.decode(val); 
			return result;
		}else{
			logger.warn("MethodRule, no this method:"+method);
		}
		
		return val;
	}
	
}
