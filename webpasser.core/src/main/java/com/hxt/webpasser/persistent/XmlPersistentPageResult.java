/*
 * 系统名称: 
 * 模块名称: webpasser.core
 * 类 名 称: XmlPersistentPageResult.java
 *   
 */
package com.hxt.webpasser.persistent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import com.hxt.webpasser.callback.ProcessCrawlUrlCallBack;
import com.hxt.webpasser.exception.SpiderException;
import com.hxt.webpasser.module.CrawlUrl;
import com.hxt.webpasser.module.LinkPageInfo;
import com.hxt.webpasser.module.PageResult;
import com.hxt.webpasser.parser.XmlPageParser;
import com.hxt.webpasser.regular.AcceptDecideRuleHandler;
import com.hxt.webpasser.spider.SpiderController;
import com.hxt.webpasser.spider.common.CrawlUrlUtil;
import com.hxt.webpasser.transport.xml.Field;
import com.hxt.webpasser.transport.xml.Page;
import com.hxt.webpasser.transport.xml.Param;
import com.hxt.webpasser.transport.xml.XmlTask;
import com.hxt.webpasser.utils.FillParamValueUtil;
import com.hxt.webpasser.utils.TrimUtil;

/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2015-9-14 <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class XmlPersistentPageResult  implements PersistentPageResult{

	//protected static Log logger= SpiderLogs.getLog(XmlPersistentPageResult.class);
	private XmlTask xmlTask;
	private SpiderController spiderController;
	//private XmlPageParser xmlPageParser;
	
	public XmlPersistentPageResult( XmlTask xmlTask,SpiderController spiderController)
	{
		this.xmlTask=xmlTask;
		this.spiderController=spiderController;
		/*
		if(spiderController.getHtmlParser() instanceof XmlPageParser){
			xmlPageParser=(XmlPageParser)spiderController.getHtmlParser();
			
		}else{
			throw new SpiderException("使用XmlPersistentPageResult了，则HtmlParser必须是或继承  XmlPageParser");
		}*/
	}
	
	public void persistent(PageResult pageResult,ProcessCrawlUrlCallBack processCrawlUrlCallBack) {
		
		Map<String,Object> returnValue = pageResult.getReturnValue();
		//if(pageResult.getPersistentPageInt()!=null)
		if(returnValue!=null)
		{
			 //Page xmlPage=xmlTask.getPages().get(pageResult.getPersistentPageInt());
			 spiderController.getSpiderTaskTriggerChain().afterParseToMap(returnValue);
			 TrimUtil.trimMap(returnValue);
			 // 存储
			 if(spiderController.getHandleResultMapInterface()!=null){
				 spiderController.getHandleResultMapInterface().handle(returnValue);
			 }else{
				 System.out.println(pageResult.getUrl()+" 没有handle处理！");
			 }
			 if(processCrawlUrlCallBack!=null){
				 processCrawlUrlCallBack.handleReturnMapValue(returnValue);
			 }
			 spiderController.getSpiderTaskTriggerChain().afterPersistent(returnValue);
			//logger.info("存储解析时间："+runTimeCost.getCostTimeMicrosecond()+"毫秒");
		}
		
	}
	

	
}
